import soco
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--action')
parser.add_argument('-i', '--ip')
parser.add_argument('-s', '--stop')
args = parser.parse_args();

sonos = soco.SoCo(args.ip)
if args.action:
  uri = args.action
  sonos.add_uri_to_queue(uri)

if args.stop:
  sonos.stop()