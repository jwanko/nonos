import soco
speakers = soco.discover()

# Display a list of speakers
for speaker in speakers:
    if speaker.is_coordinator:
        print "%s,%s" % (speaker.player_name, speaker.ip_address)
