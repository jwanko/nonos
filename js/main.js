'use strict';

var angular;

angular.$externalBroadcast = function (selector, event, message) {
    var scope = angular.element(selector).scope();

    scope.$apply(function () {
        scope.$broadcast(event, message);
    });
};

angular.module('nonosApp', [])
  .controller('listController', ['$scope', function($scope) {

    $scope.addTodo = function() {
      $scope.todos.push({text:$scope.todoText, done:false});
      $scope.todoText = '';
    };

  }]);

function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = 0;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;
        var opt = $(this.opts[0]);
        this.val = opt.text();
        this.index = opt.index();
        this.placeholder.text(this.val);

        obj.dd.on('click', function(event){
            event.stopPropagation();
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
};


$(function(){
    var dd = new DropDown($('#dd'));

    $('.songlink').click(function(e){
        e.preventDefault();
        var link = $(dd.opts[dd.getIndex()]).find('a');
        var ip = link.attr('data-ip');
        var filename = $(this).attr('data-filename');
        $.get( "add.php", { i: ip, f: filename } );

    });

    $('#file_upload').uploadifive({
        'buttonClass'  : 'icon',
        'uploadScript' : 'uploadifive.php',
        'width' : 650,
        'height' : 200,
        'onUpload'     : function(filesToUpload) {
            $('.loaderbar').transition({
                width: '100%'
            }, 3000, function(){
                $('.loaderbar').animate({
                    opacity: 0
                }, 1000, function(){
                    $('.loaderbar').css({
                        width: 0,
                        opacity: 1
                    });
                    location.reload();
                });
            });
        }
    });

});