<?php if(!function_exists('_log')){
  function _log( $message ) {
    if( is_array( $message ) || is_object( $message ) ){
    error_log( print_r( $message, true ) );
    } else {
    error_log( $message );
    }
  }
} ?>

<!DOCTYPE html>
<html class="no-js" ng-app="nonosApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/uploadifive.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.7/angular.min.js"></script>
    </head>
    <body>

<?php
  exec('python getRooms.py', $result);
  rsort($result);
  ?>

        <div class="container">
          <div class="header">
            <h1 class="logo">NONOS</h1>
          </div>
          <div class="uploader">
          <form>
            <input id="file_upload" name="file_upload" type="file" multiple="true">
          </form>
          <span class="text">Drag files or click icon to upload</span>
          <div class="loaderbar"></div>
          </div>
          <div class="sonos-rooms">
             <div id="dd" class="wrapper-dropdown wrapper-dropdown-1" tabindex="1">
                <span>Select a Room</span>
                <ul class="dropdown">

                <?php

                foreach ($result as $room) {
                  $roomPieces = explode(",", $room); ?>
                  <li><a href="#" data-roomname="<?php echo $roomPieces[0]; ?>" data-ip="<?php echo $roomPieces[1]; ?>"><?php echo $roomPieces[0]; ?></a></li>
                <?php } ?>
                </ul>
            </div>
          </div>
          <?php $dir    = 'uploads/';
            $files = scandir($dir);?>
          <div class="list" ng-scope="listController" ng-init="songs =[<?php foreach ($files as $file) { if ($file != ".DS_Store" && $file != "." && $file != "..") { ?>{ filename:'<?php echo $file; ?>', prettyName: '<?php echo substr($file, 0, -4); ?>' },<?php } } ?>]">
            <input type="text" class="search" ng-model="searchText">
            <ul class="songs">
              <li class="song" ng-repeat="song in songs | filter:searchText">
                <a href="#" data-filename="{{song.filename}}" class="songlink">{{song.prettyName}}</a>
              </li>
            </ul>
          </div>

        </div>
        <div class="debug-footer">Built by Joe Wanko || Build: nonos_3.0.134.<?php echo date("md.y");   ?> </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="js/jquery.uploadifive.js" type="text/javascript"></script>
        <script src="js/app.js"></script>
        <script src="js/main.js"></script>

<!--           <script type="text/javascript">
            $(function() {
              $('#file_upload').uploadifive({
                'buttonClass'  : 'icon',
                'uploadScript' : 'uploadifive.php',
                'width' : 650,
                'height' : 200,
                'onUpload'     : function(filesToUpload) {
                  alert(filesToUpload + ' files will be uploaded.');
                }
              });
            });
          </script> -->
    </body>
</html>
